namespace OlympicGames.Domain
{
    public class Competition
    {
        public int Id { get; set; }
        public SportType SportType { get; set; }
        public int Teams { get; set; }
        public int Matches { get; set; }
    }
}