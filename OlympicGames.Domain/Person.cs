﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OlympicGames.Domain
{
    public abstract class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Country { get; set; }
        public Gender Gender { get; set; }

        // constructors
        public Person() { }

        public Person(string name, string surname, string country, Gender gender)
        {
            this.Name = name;
            this.Surname = surname;
            this.Country = country;
            this.Gender = gender;
        }
        // returns info about person as string
        public virtual string Summary() { return $"{Name} {Surname} {Gender} {Country} "; }
    }
}
