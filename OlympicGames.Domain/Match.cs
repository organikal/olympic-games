using System;

namespace OlympicGames.Domain
{
    public class Match
    {
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public Country FirstTeam { get; set; }
        public Country SecondTeam { get; set; }
        public int? FirstTeamPoints { get; set; }    // nullable
        public int? SecondTeamPoints { get; set; }   // nullable

        // returns the name of the winning team
        public string Winners()
        {
            if (FirstTeamPoints != null || SecondTeamPoints != null)
            {
                if (FirstTeamPoints != SecondTeamPoints)
                {
                    if (FirstTeamPoints > SecondTeamPoints)
                        return FirstTeam.CountryCode;
                    else
                        return SecondTeam.CountryCode;
                }
                else
                    return "Draw";
            }
            else
                return "TBA";
        }

    }
}