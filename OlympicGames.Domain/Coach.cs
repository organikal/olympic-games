namespace OlympicGames.Domain
{
    public class Coach : Person
    {
        public int Id { get; set; }
        public string Nationality { get; set; }

        // constructors
        public Coach () { }

        public Coach(string name, string surname, string country, Gender gender, string nationality)
            : base(name, surname, country, gender)
        {
            this.Nationality = nationality;
        }

        // returns info about person as string
        public override string Summary() { return $"{base.Summary()} {Nationality}"; }
    }
}