namespace OlympicGames.Domain
{
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }         // eg.: Lithuania
        public string CountryCode { get; set; }  // eg.: LTU

        // constructors
        public Country() { }

        public Country(string name, string countryCode)
        {
            this.Name = name;
            this.CountryCode = countryCode;
        }
    }
}