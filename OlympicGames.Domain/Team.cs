using System;
using System.Collections.Generic;

namespace OlympicGames.Domain
{
    public class Team
    {
        public int Id { get; set; }
        public Coach Coach { get; set; }
        public string Country { get; set; }
        public List<Sportsperson> Sportspeople { get; set; }

        // constructors
        public Team () { }

        public Team(Coach coach, string country, List<Sportsperson> sportspeople)
        {
            this.Coach = coach;
            this.Country = country;
            this.Sportspeople = sportspeople;
        }
    }
}