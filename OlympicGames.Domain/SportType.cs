namespace OlympicGames.Domain
{
    public enum SportType
    {
        Rugby,
        Football,
        FieldHockey,
        Basketball,
        Volleyball,
        Handball,
        Waterpolo,
        Curling,
        Bobsleigh 
    };
}