namespace OlympicGames.Domain
{
    public class Sportsperson : Person
    {
        public int Id { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }

        // constructors
        public Sportsperson() { }

        public Sportsperson(string name, string surname, string country, Gender gender, int height, int weight)
            : base(name, surname, country, gender)
        {
            this.Height = height;
            this.Weight = weight;
        }

        // returns info about person as string
        public override string Summary() { return $"{base.Summary()} {Height}cm {Weight}kg"; }
    }
}