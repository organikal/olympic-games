using System;

namespace OlympicGames.Domain
{
    public class Article
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime Created { get; set; }

        // constructors
        public Article(){}

        public Article(string text, DateTime created)
        {
            this.Text = text;
            this.Created = created;
        }
    }
}