﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using OlympicGames.Domain;

namespace OlympicGames.DataAccess
{
    internal class OlympicGamesInitializer : DropCreateDatabaseAlways<OlympicGamesContext>
    {
        // MANO LENTELIŲ TURINYS
        protected override void Seed(OlympicGamesContext context)
        {
            // ----------- ŽAIDĖJAI
            var player1 = new Sportsperson("Benas", "Bagdanovas", "Lietuva", Gender.Male, 183, 88);
            var player2 = new Sportsperson("Kristijonas", "Kerekeš", "Lietuva", Gender.Male, 182, 75);
            var player3 = new Sportsperson("Valdemar", "Siličenko", "Lietuva", Gender.Male, 181, 82);
            var player4 = new Sportsperson("Sigita", "Mazonaitė", "Portugalija", Gender.Female, 170, 56);
            
            var sportspersons1 = new List<Sportsperson> {player1, player2};
            var sportspersons2 = new List<Sportsperson> {player3, player4};

            // išsaugau
            sportspersons1.ForEach(s => context.Sportspersons.AddOrUpdate(s));
            context.SaveChanges();
            sportspersons2.ForEach(s => context.Sportspersons.AddOrUpdate(s));
            context.SaveChanges();



            // ----------- TRENERIAI
            Country country1 = new Country("Lithuania", "LTU");
            Country country2 = new Country("France", "FR");

            var coach1 = new Coach("Agnė", "Raminauskienė", "Lithuania", Gender.Female, "LTU");
            var coach2 = new Coach("Jaroslav", "Jusel", "France", Gender.Male, "FR");

            var coaches = new List<Coach> {coach1, coach2};

            // išsaugau
            coaches.ForEach(s => context.Coaches.AddOrUpdate(s));
            context.SaveChanges();



            // ----------- KOMANDOS
            var team1 = new Team(coach1, "Lithuania", sportspersons1);
            var team2 = new Team(coach2, "France", sportspersons2);

            var teams = new List<Team> {team1, team2};

            // išsaugau
            teams.ForEach(s => context.Teams.AddOrUpdate(s));
            context.SaveChanges();



            // ----------- ŠALYS
            var countries = new List<Country> {country1, country2};

            // išsaugau
            countries.ForEach(s => context.Countries.AddOrUpdate(s));
            context.SaveChanges();


            // ----------- STRAIPSNIAI
            var article1 = new Article("Pirmasis straipsnis.", DateTime.Now);
            var article2 = new Article("Antrasis tekstas.", DateTime.Now);

            var articles = new List<Article> {article1, article2};

            // išsaugau
            articles.ForEach(s => context.Articles.AddOrUpdate(s));
            context.SaveChanges();

        }
    }
}
