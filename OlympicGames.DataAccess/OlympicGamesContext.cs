﻿using System.Data.Entity;
using OlympicGames.Domain;

namespace OlympicGames.DataAccess
{
    public class OlympicGamesContext : DbContext
    {
        public OlympicGamesContext() : base()
        {
            // ČIA SUKURSIU LENTELEI NARIUS 
            Database.SetInitializer(new OlympicGamesInitializer());
        }

        // MANO LENTELĖS STULPELIAI
        public DbSet<Sportsperson> Sportspersons { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Article> Articles { get; set; }
    }
}
