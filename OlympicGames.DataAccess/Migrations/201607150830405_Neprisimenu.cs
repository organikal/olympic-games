namespace OlympicGames.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Neprisimenu : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Teams", "Country_Id", "dbo.Countries");
            DropIndex("dbo.Teams", new[] { "Country_Id" });
            AddColumn("dbo.Teams", "Country", c => c.String());
            DropColumn("dbo.Teams", "Country_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Teams", "Country_Id", c => c.Int());
            DropColumn("dbo.Teams", "Country");
            CreateIndex("dbo.Teams", "Country_Id");
            AddForeignKey("dbo.Teams", "Country_Id", "dbo.Countries", "Id");
        }
    }
}
