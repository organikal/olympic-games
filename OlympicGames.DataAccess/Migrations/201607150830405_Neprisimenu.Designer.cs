// <auto-generated />
namespace OlympicGames.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Neprisimenu : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Neprisimenu));
        
        string IMigrationMetadata.Id
        {
            get { return "201607150830405_Neprisimenu"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
