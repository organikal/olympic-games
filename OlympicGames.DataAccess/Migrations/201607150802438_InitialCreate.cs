namespace OlympicGames.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Articles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Coaches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nationality = c.String(),
                        Name = c.String(),
                        Surname = c.String(),
                        Country = c.String(),
                        Gender = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CountryCode = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sportspersons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Height = c.Int(nullable: false),
                        Weight = c.Int(nullable: false),
                        Name = c.String(),
                        Surname = c.String(),
                        Country = c.String(),
                        Gender = c.Int(nullable: false),
                        Team_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Teams", t => t.Team_Id)
                .Index(t => t.Team_Id);
            
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Coach_Id = c.Int(),
                        Country_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Coaches", t => t.Coach_Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.Coach_Id)
                .Index(t => t.Country_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sportspersons", "Team_Id", "dbo.Teams");
            DropForeignKey("dbo.Teams", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.Teams", "Coach_Id", "dbo.Coaches");
            DropIndex("dbo.Teams", new[] { "Country_Id" });
            DropIndex("dbo.Teams", new[] { "Coach_Id" });
            DropIndex("dbo.Sportspersons", new[] { "Team_Id" });
            DropTable("dbo.Teams");
            DropTable("dbo.Sportspersons");
            DropTable("dbo.Countries");
            DropTable("dbo.Coaches");
            DropTable("dbo.Articles");
        }
    }
}
