﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using OlympicGames.Domain;

namespace OlympicGames.DataAccess
{
    public class CountryDataService
    {
        public Country Get(int id)
        {
            using (var context = new OlympicGamesContext())
            {
                return context.Countries.Where(s => s.Id == id).FirstOrDefault<Country>();
            }
        }

        public List<Country> GetAll()
        {
            using (var context = new OlympicGamesContext())
            {
                return context.Countries.ToList();
            }
        }

        public void Add(Country country)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Countries.AddOrUpdate(country);
                context.SaveChanges();
            }
        }

        public void Update(Country country)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Countries.AddOrUpdate(country);
                context.SaveChanges();
            }
        }

        public void Remove(int id)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Countries.Remove(context.Countries.Where(s => s.Id == id).FirstOrDefault<Country>());
                context.SaveChanges();
            }
        }
    }
}
