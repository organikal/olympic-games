﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using OlympicGames.Domain;

namespace OlympicGames.DataAccess
{
    public class CoachDataService
    {
        public Coach Get(int id)
        {
            using (var context = new OlympicGamesContext())
            {
                return context.Coaches.Where(a => a.Id == id).FirstOrDefault<Coach>();
            }
        }

        public List<Coach> GetAll()
        {
            using (var context = new OlympicGamesContext())
            {
                return context.Coaches.ToList();
            }
        }

        public void Add(Coach coach)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Coaches.AddOrUpdate(coach);
                context.SaveChanges();
            }
        }

        public void Update(Coach coach)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Coaches.AddOrUpdate(coach);
                context.SaveChanges();
            }
        }

        public void Remove(int Id)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Coaches.Remove(context.Coaches.Where(s => s.Id == Id).FirstOrDefault<Coach>());
                context.SaveChanges();
            }
        }
    }
}
