﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using OlympicGames.Domain;

namespace OlympicGames.DataAccess
{
    public class ArticleDataService
    {
        public Article Get(int id)
        {
            using (var context = new OlympicGamesContext())
            {
                return context.Articles.Where(a => a.Id == id).FirstOrDefault<Article>();
            }
        }

        public List<Article> GetAll()
        {
            using (var context = new OlympicGamesContext())
            {
                return context.Articles.ToList();
            }
        }

        public void Add(Article article)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Articles.AddOrUpdate(article);
                context.SaveChanges();
            }
        }

        public void Update(Article article)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Articles.AddOrUpdate(article);
                context.SaveChanges();
            }
        }

        public void Remove(int Id)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Articles.Remove(context.Articles.Where(s => s.Id == Id).FirstOrDefault<Article>());
                context.SaveChanges();
            }
        }
    }
}
