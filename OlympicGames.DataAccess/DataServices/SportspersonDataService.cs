﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using OlympicGames.Domain;

namespace OlympicGames.DataAccess
{
    public class SportspersonDataService
    {
        public Sportsperson Get(int id)
        {
            using (var context = new OlympicGamesContext())
            {
                return context.Sportspersons.Where(s => s.Id == id).FirstOrDefault<Sportsperson>();
            }
        }

        public List<Sportsperson> GetAll()
        {
            using (var context = new OlympicGamesContext())
            {
                return context.Sportspersons.ToList();
            }
        }

        public void Add(Sportsperson sportsperson)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Sportspersons.AddOrUpdate(sportsperson);
                context.SaveChanges();
            }
        }

        public void Update(Sportsperson sportsperson)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Sportspersons.AddOrUpdate(sportsperson);
                context.SaveChanges();
            }
        }

        public void Remove(int id)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Sportspersons.Remove(context.Sportspersons.Where(s => s.Id == id).FirstOrDefault<Sportsperson>());
                context.SaveChanges();
            }
        }
    }
}
