﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using OlympicGames.Domain;

namespace OlympicGames.DataAccess
{
    public class TeamDataService
    {
        public Team Get(int id)
        {
            using (var context = new OlympicGamesContext())
            {
                return context.Teams.Where(a => a.Id == id).FirstOrDefault<Team>();
            }
        }

        public List<Team> GetAll()
        {
            using (var context = new OlympicGamesContext())
            {
                return context.Teams.ToList();
            }
        }

        public void Add(Team team)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Teams.AddOrUpdate(team);
                context.SaveChanges();
            }
        }

        public void Update(Team team)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Teams.AddOrUpdate(team);
                context.SaveChanges();
            }
        }

        public void Remove(int Id)
        {
            using (var context = new OlympicGamesContext())
            {
                context.Teams.Remove(context.Teams.Where(s => s.Id == Id).FirstOrDefault<Team>());
                context.SaveChanges();
            }
        }
    }
}
