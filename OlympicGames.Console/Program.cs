﻿using System;
using System.Collections.Generic;
using System.Net.Configuration;
using OlympicGames.DataAccess;
using OlympicGames.Domain;

namespace OlympicGames.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            ArticleDataService ArticleManager = new ArticleDataService();

            Article article1 = new Article("First straipsnis.", DateTime.Now);
            Article article2 = new Article("Second straipsnis.", DateTime.Now);
            ArticleManager.Add(article1);
            ArticleManager.Add(article2);

            List<Article> list;
            list = ArticleManager.GetAll();

            foreach (var item in list)
            {
                System.Console.WriteLine(item.Text);
            }

            System.Console.WriteLine("Done!");

            System.Console.ReadKey();
        }
    }
}
